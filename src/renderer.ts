import { IpcRendererEvent, ipcRenderer } from "electron";
import { ExceptionFiles, Settings, Stats } from "./Interface";

window.addEventListener("DOMContentLoaded", () => {
	document.getElementById("originDir").addEventListener("click", () => {
		ipcRenderer.send("originDirDialog");
	});

	document.getElementById("targetDir").addEventListener("click", () => {
		ipcRenderer.send("targetDirDialog");
	});

	document.getElementById("resetSettings").addEventListener("click", () => {
		resetSettings(true);
	});

	document.getElementById("keepDuplicates").addEventListener("click", () => {
		if (isChecked("keepDuplicates") === true) {
			removeAttr("duplicateFileBlock", "hidden");
		} else {
			hideElem("duplicateFileBlock");
		}
	});

	document.getElementById("organizeDir").addEventListener("click", () => {
		if (isChecked("organizeDir") === true) {
			removeAttr("organizeDirBlock", "hidden");
		} else {
			hideElem("organizeDirBlock");
		}
	});

	document.getElementById("chooseFileTypes").addEventListener("click", () => {
		if (isChecked("chooseFileTypes") === true) {
			removeAttr("allowedTypesBlock", "hidden");
		} else {
			hideElem("allowedTypesBlock");
		}
	});

	document.getElementById("initFileMigration").addEventListener("click", () => {
		const args: Settings = getManagerArgs();
		if (args && args.originDir && args.targetDir && args.originDir.length > 0 && args.targetDir.length > 0) {
			removeAttr("progressBarCont", "hidden");

			(document.getElementById("resetSettings") as HTMLInputElement).disabled = true;

			ipcRenderer.send("initFileMigration", args);
		} else if (!args.originDir || args.originDir.length === 0) {
			showErrorMsg({ name: "initFileMigration", message: `originDir is empty` });
		} else if (!args.targetDir || args.targetDir.length === 0) {
			showErrorMsg({ name: "initFileMigration", message: `targetDir is empty` });
		} else {
			showErrorMsg({ name: "initFileMigration", message: `originDir and targetDir are empty ` });
		}
	});
});

ipcRenderer.on("selectOriginDir", (event: IpcRendererEvent, selectOriginDir: string) => {
	addValue("selectOriginDir", selectOriginDir);
	removeAttr("targetContainer", "hidden");
});

ipcRenderer.on("selectTargetDir", (event: IpcRendererEvent, selectTargetDir: string) => {
	addValue("selectTargetDir", selectTargetDir);
	removeAttr("startContainer", "hidden");
});

ipcRenderer.on("updateBarProgress", (event: IpcRendererEvent, percent: string) => {
	updateProgressBar(percent);
});

ipcRenderer.on("updateStats", (event: IpcRendererEvent, stats: Stats) => {
	updateStats(stats);
});

ipcRenderer.on("errorDialog", (event: IpcRendererEvent, error: Error) => {
	resetSettings();
});

ipcRenderer.on("finishedMigration", (event: IpcRendererEvent, exceptionFiles: ExceptionFiles[]) => {
	if (exceptionFiles instanceof Array && exceptionFiles.length > 0) {
		const fileErrors: string = exceptionFiles.reduce((acc, elem) => {
			if (elem && elem.entity && elem.entity.path && typeof elem.entity.path === "string") {
				acc += `${elem.entity.path} \n\n`;
			}
			return acc;
		}, "" as string);
		showErrorMsg({ name: "finishedMigration errors", message: fileErrors });
	} else {
		removeAttr("resetSettings", "disabled");
	}
	resetSettings();
});

function getManagerArgs(): Settings {
	const args: Settings = {
		originDir: getValue("selectOriginDir"),
		targetDir: getValue("selectTargetDir"),
		deleteOrigin: isChecked("deleteOriginFiles"),
		renameWithDate: isChecked("renameWithDate"),
	};
	if (isChecked("keepDuplicates") === true) {
		args.duplicatesPattern = getValue("duplicatePattern");
	}
	if (isChecked("chooseFileTypes") === true) {
		// todo: validate types
		args.allowedTypes = getValue("allowedTypes").split(",");
	}
	if (isChecked("organizeDir") === true) {
		if (isChecked("day")) {
			args.organizeDirBy = "day";
		} else if (isChecked("month")) {
			args.organizeDirBy = "month";
		} else if (isChecked("year")) {
			args.organizeDirBy = "year";
		} else {
			showErrorMsg({ name: "organizeDir", message: "error not supported" });
		}
	}
	return args;
}

function showErrorMsg(error: Error) {
	ipcRenderer.send("showErrorDialog", error);
}

function removeAttr(elemId: string, attr: string): void {
	(document.getElementById(`${elemId}`) as HTMLInputElement).removeAttribute(`${attr}`);
}

function isChecked(elemId: string): boolean {
	return !!(document.getElementById(`${elemId}`) as HTMLInputElement).checked;
}

function addValue(elemId: string, value: string): void {
	(document.getElementById(`${elemId}`) as HTMLInputElement).value = value;
}

function getValue(elemId: string): string {
	return (document.getElementById(`${elemId}`) as HTMLInputElement).value;
}

function hideElem(elemId: string): void {
	(document.getElementById(`${elemId}`) as HTMLInputElement).hidden = true;
}

function resetSettings(hideStats: boolean = false): void {
	const elementId = ["organizeDirBlock", "duplicateFileBlock", "allowedTypesBlock"];
	if (hideStats === true) {
		elementId.push("progressBarCont", "fileStatistics", "startContainer", "targetContainer");
		addValue("selectOriginDir", "Origin path selected");
		addValue("selectTargetDir", "Target path selected");
	}

	elementId.forEach((elemId: string) => {
		const element: HTMLInputElement = document.getElementById(elemId) as HTMLInputElement;
		if (element) {
			hideElem(elemId);
		}
	});

	Array.from(document.getElementsByTagName("input") as HTMLCollectionOf<HTMLInputElement>).forEach((input: { checked: boolean; id: string }) => {
		input.checked = false;
	});
}

function updateProgressBar(percent: string): void {
	(document.getElementById("progressBarPercent") as HTMLInputElement).innerHTML = percent;
	(document.getElementById("progressBar") as HTMLInputElement).style.width = `${percent}%`;
	if (parseInt(percent, 2) === 100) {
		setTimeout(() => {
			removeAttr("endMsg", "hidden");
		}, 250);
	}
}

function updateStats(stats: Stats) {
	removeAttr("fileStatistics", "hidden");
	(document.getElementById("totalFiles") as HTMLInputElement).innerHTML = stats.totalFiles.toString();
	(document.getElementById("totalSize") as HTMLInputElement).innerHTML = `${stats.totalSize.toString()}  MB`;
}
