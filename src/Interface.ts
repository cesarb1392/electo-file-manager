interface Settings {
	originDir: string;
	targetDir: string;
	renameWithDate: boolean;
	deleteOrigin: boolean;
	duplicatesPattern?: string;
	allowedTypes?: string[];
	organizeDirBy?: string;
}

interface Stats {
	totalFiles: number;
	totalSize: number;
}

interface Entity {
	isDir?: boolean;
	path: string;
	name: string;
	size: number;
	createdAt: Date;
}

interface ExceptionFiles {
	error: string;
	entity: Entity;
}

export { ExceptionFiles, Entity, Stats, Settings };
