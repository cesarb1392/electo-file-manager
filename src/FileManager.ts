import { glob } from "glob";
import { exists, lstat, rename, copyFile, mkdir } from "fs";
import { promisify } from "util";
import { extname, join, basename } from "path";
import IpcMainEvent = Electron.IpcMainEvent;
import { Entity, ExceptionFiles, Settings } from "./Interface";

const fsExists = promisify(exists);

const ORGANIZE_BY: { [key: string]: string } = {
	DAY: "day",
	MONTH: "month",
	YAER: "year",
};

export default class Manager {
	private readonly originDir: string;
	private readonly targetDir: string;
	private readonly deleteOrigin: boolean;
	private readonly renameWithDate: boolean;
	private readonly duplicatesPattern: string;
	private readonly exceptionFiles: ExceptionFiles[];
	private readonly organizeDirBy: string;
	private allowedTypes: string[];
	private event: IpcMainEvent;

	constructor(args: Settings, event: IpcMainEvent) {
		this.targetDir = args.targetDir;
		this.originDir = args.originDir;
		this.deleteOrigin = !!args.deleteOrigin;
		this.renameWithDate = !!args.renameWithDate;
		this.duplicatesPattern = args.duplicatesPattern || undefined;
		this.event = event;
		this.exceptionFiles = [];

		// todo: validate types
		this.allowedTypes = args.allowedTypes && args.allowedTypes.length > 0 ? args.allowedTypes : [];

		this.organizeDirBy = args.organizeDirBy && Object.values(ORGANIZE_BY).includes(args.organizeDirBy) ? args.organizeDirBy : undefined;
	}

	async init(): Promise<ExceptionFiles[]> {
		try {
			const originDirTotalFiles: Entity[] = await this.getOriginFiles();
			this.updateStats(originDirTotalFiles);
			if (originDirTotalFiles.length > 0) {
				await this.migrateFiles(originDirTotalFiles);
			} else {
				this.updateProgressBar(1, 1);
			}
			return this.exceptionFiles;
		} catch (e) {
			throw new Error(e);
		}
	}

	private async getOriginFiles(): Promise<Entity[]> {
		try {
			const files: Entity[] = await this.getFilesInPath(this.originDir);
			return this.sortFiles(files, "name").reduce((acc: Entity[], entity: Entity) => {
				if (!entity.isDir) {
					acc.push({
						path: entity.path,
						name: entity.name,
						createdAt: entity.createdAt,
						size: entity.size,
					});
				}
				return acc;
			}, [] as Entity[]);
		} catch (e) {
			throw new Error(e);
		}
	}

	private updateStats(files: Entity[]): void {
		const totalSize: number = files.reduce((acc: number, file: Entity) => {
			if (file.size) {
				acc += file.size;
			}
			return acc;
		}, 0 as number);

		this.event.sender.send("updateStats", {
			totalFiles: files.length || 0,
			totalSize: totalSize ? totalSize / 1000000 : 0, // MB
		});
	}

	private async getFilesInPath(path: string): Promise<Entity[]> {
		const files: Entity[] = [];
		let filePattern: string = `${path}/**/*`;
		if (this.allowedTypes.length > 0) {
			filePattern = `${path}/**/*.+(${this.allowedTypes.join("|")})`;
		}
		const filePaths: string[] = await promisify(glob)(filePattern);

		for (const filePath of filePaths) {
			const fileStat = await promisify(lstat)(filePath);
			files.push({
				isDir: fileStat.isDirectory(),
				path: filePath,
				name: basename(filePath),
				createdAt: fileStat.birthtime,
				size: fileStat.size,
			});
		}
		return files;
	}

	private sortFiles(files: Entity[], type: string): Entity[] {
		switch (type) {
			case "date":
				return files.sort((a, b) => (a.createdAt > b.createdAt ? 1 : -1));
			case "name":
				return files.sort((a, b) => (a.name > b.name ? 1 : -1));
			default:
				return files;
		}
	}

	private async migrateFiles(originDirFiles: Entity[]): Promise<void> {
		try {
			await this.createDir(this.targetDir);
			await this.processFiles(originDirFiles);
		} catch (e) {
			throw new Error(e);
		}
	}

	private async processFiles(originDirFiles: Entity[]): Promise<void> {
		for (let i = 0; i < originDirFiles.length; ++i) {
			let fileName: string = this.renameWithDate ? this.getFileNameFromDate(originDirFiles[i].createdAt, originDirFiles[i].name) : originDirFiles[i].name;
			if (this.duplicatesPattern) {
				fileName = await this.renameFileNameIfExists(fileName, 0, extname(fileName));
			}

			let targetDirFile: string = join(this.targetDir, fileName);
			if (this.organizeDirBy) {
				targetDirFile = join(await this.organizeDir(originDirFiles[i]), fileName);
			}

			const managerMode = this.deleteOrigin ? await promisify(rename) : await promisify(copyFile);
			try {
				if (originDirFiles[i].path && (await this.pathExists(originDirFiles[i].path)) === true) {
					managerMode(originDirFiles[i].path, targetDirFile);
				} else {
					throw new Error(`File path doesn't exists in filesystem`);
				}
			} catch (error) {
				this.exceptionFiles.push({ error, entity: originDirFiles[i] });
				continue;
			}
			this.updateProgressBar(i + 1, originDirFiles.length);
		}
	}

	private async renameFileNameIfExists(oldFileName: string, copyNumber, fileExtension: string): Promise<string> {
		let newFileName: string = oldFileName;
		if ((await fsExists(join(this.targetDir, oldFileName))) === true) {
			++copyNumber;
			if (newFileName.includes(this.duplicatesPattern)) {
				newFileName = newFileName.replace(`${this.duplicatesPattern} ${copyNumber - 1}`, `${this.duplicatesPattern} ${copyNumber}`);
			} else {
				newFileName = `${newFileName.replace(fileExtension, "")}${this.duplicatesPattern} ${copyNumber}${fileExtension}`;
			}
			return this.renameFileNameIfExists(newFileName, copyNumber, fileExtension);
		}
		return newFileName;
	}

	private async organizeDir(entity: Entity): Promise<string> {
		let targetDir: string;
		switch (this.organizeDirBy) {
			case ORGANIZE_BY.DAY:
				targetDir = await this.createDirByDate(entity, ORGANIZE_BY.DAY);
				break;
			case ORGANIZE_BY.MONTH:
				targetDir = await this.createDirByDate(entity, ORGANIZE_BY.MONTH);
				break;
			case ORGANIZE_BY.YEAR:
				targetDir = await this.createDirByDate(entity, ORGANIZE_BY.YEAR);
				break;
			default:
				break;
		}
		return targetDir;
	}

	private async createDirByDate(entity: Entity, organizeBy: string): Promise<string> {
		const day: string = entity.createdAt.getDay().toString(),
			month: string = new Intl.DateTimeFormat("en", { month: "short" }).format(entity.createdAt),
			year: string = entity.createdAt.getFullYear().toString();

		if ((await fsExists(join(this.targetDir, year))) === false) {
			await this.createDir(join(this.targetDir, year));
		}

		if (organizeBy !== ORGANIZE_BY.YEAR && (await fsExists(join(this.targetDir, year, month))) === false) {
			await this.createDir(join(this.targetDir, year, month));
		}

		if (organizeBy !== ORGANIZE_BY.YEAR && organizeBy !== ORGANIZE_BY.MONTH && (await fsExists(join(this.targetDir, year, month, day))) === false) {
			await this.createDir(join(this.targetDir, year, month, day));
		}

		let targetDir: string = join(this.targetDir, year);
		if (organizeBy === ORGANIZE_BY.MONTH) {
			targetDir = join(this.targetDir, year, month);
		} else if (organizeBy === ORGANIZE_BY.DAY) {
			targetDir = join(this.targetDir, year, month, day);
		}

		return targetDir;
	}

	private async createDir(folderPath: string): Promise<void> {
		if ((await fsExists(folderPath)) === false) {
			promisify(mkdir)(folderPath);
		}
	}

	private async pathExists(path: string): Promise<boolean> {
		return (await fsExists(path)) === true;
	}

	private getFileNameFromDate(date: Date, fileName: string): string {
		const year: string = date.getDay().toString(),
			month: string = new Intl.DateTimeFormat("en", { month: "short" }).format(date),
			day: string = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(date),
			hour: string = date.getHours().toString(),
			minute: string = date.getMinutes().toString(),
			fileType: string = extname(fileName);
		return `${minute}h - ${hour}min -- ${day} - ${month} - ${year}${fileType}`;
	}

	private updateProgressBar(i: number, total: number): void {
		return this.event.sender.send("updateBarProgress", Math.round((100 * i) / total));
	}
}
