import { app, BrowserWindow, dialog, ipcMain, IpcMainEvent, OpenDialogReturnValue } from "electron";
import * as path from "path";
import Manager from "./FileManager";
import { ExceptionFiles, Settings } from "./Interface";

let mainWindow: Electron.BrowserWindow;

export const createWindow = (): void => {
	mainWindow = new BrowserWindow({
		fullscreen: false,
		webPreferences: {
			nodeIntegration: true,
			// 	preload: path.join(__dirname, 'preload.js'),
		},
		titleBarStyle: "hidden",
		frame: true,
		width: 700,
		height: 700,
		icon: path.join(__dirname, "public", "assets", "favicon-mac.ico"),
	});

	if (process.env.NODE_ENV === "development") {
		mainWindow.webContents.openDevTools();
	} else {
		process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = "1";
	}

	mainWindow.loadFile(path.join(__dirname, "..", "public", "index.html"));

	ipcMain.on("originDirDialog", (event: IpcMainEvent): void => {
		dialog
			.showOpenDialog({ properties: ["showHiddenFiles", "openDirectory"] })
			.then((obj: OpenDialogReturnValue) => {
				if (obj.filePaths && obj.filePaths.length === 1) {
					return event.sender.send("selectOriginDir", obj.filePaths[0]);
				}
			})
			.catch((error: Error) => {
				dialog.showErrorBox(error.name, error.message);
				return event.sender.send("errorDialog");
			});
	});

	ipcMain.on("targetDirDialog", (event: IpcMainEvent): void => {
		dialog
			.showOpenDialog({ properties: ["showHiddenFiles", "openDirectory"] })
			.then((obj: OpenDialogReturnValue) => {
				if (obj.filePaths && obj.filePaths.length === 1) {
					return event.sender.send("selectTargetDir", obj.filePaths[0] || null);
				}
			})
			.catch((error: Error) => {
				dialog.showErrorBox(error.name, error.message);
				return event.sender.send("errorDialog");
			});
	});

	ipcMain.on(
		"initFileMigration",
		async (event: IpcMainEvent, args: Settings): Promise<void> => {
			new Manager(args, event)
				.init()
				.then((exceptionFiles: ExceptionFiles[]) => event.sender.send("finishedMigration", exceptionFiles))
				.catch((error: Error) => {
					dialog.showErrorBox(error.name, error.message);
					return event.sender.send("errorDialog");
				});
		}
	);

	ipcMain.on("showErrorDialog", (event: IpcMainEvent, error: Error): void => {
		if (error && error.name && error.message) {
			dialog.showErrorBox(error.name, error.message);
		}
		return event.sender.send("errorDialog", error);
	});

	mainWindow.webContents.on("new-window", (event: Event, url: string, frameName: string, disposition, options, additionalFeatures): void => {
		if (frameName === "modal") {
			// open window as modal
			Object.assign(options, {
				modal: true,
				parent: mainWindow,
				width: 100,
				titleBarStyle: "hidden",
				frame: true,
				height: 100,
			});
			new BrowserWindow(options);
		}
	});

	mainWindow.on("closed", (): void => {
		mainWindow = null;
	});
};

app.on("ready", createWindow);

app.on("window-all-closed", (): void => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

app.on("activate", (): void => {
	if (mainWindow === null) {
		createWindow();
	}
});

app.on("browser-window-focus", () => {
	if (process.platform === "darwin") {
		app.dock.setBadge("");
	}
});

switch (process.argv[1]) {
	case "--squirrel-install":
	case "--squirrel-updated":
	case "--squirrel-obsolete":
		app.quit();
}
