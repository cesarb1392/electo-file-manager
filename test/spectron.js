const path = require("../node_modules/path/path");
const Application = require("spectron").Application;

module.exports = function initialiseSpectron() {
	let electronPath = path.join(__dirname, "..", "node_modules", ".bin", "electron");
	const appPath = path.join(__dirname, "..", "bin", "main.js");
	if (process.platform === "win32") {
		electronPath += ".cmd";
	}

	return new Application({
		path: electronPath,
		args: [appPath],
		env: {
			ELECTRON_ENABLE_LOGGING: true,
			ELECTRON_ENABLE_STACK_DUMPING: true,
			NODE_ENV: "development",
		},
		startTimeout: 1000,
		chromeDriverLogPath: "./chromedriverlog.txt",
	});
};
