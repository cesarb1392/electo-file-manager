const chaiAsPromised = require("chai-as-promised");
const chai = require("chai");
chai.should();
chai.use(chaiAsPromised);

describe("Application launch", () => {
	const initialiseSpectron = require("./spectron");
	const app = initialiseSpectron();

	beforeEach(() => {
		chaiAsPromised.transferPromiseness = app.transferPromiseness;
		return app.start();
	});

	afterEach(() => {
		if (app && app.isRunning()) {
			return app.stop();
		}
	});

	const selectOriginButton = "button*=Select Origin Dir";
	const selectTargetButton = "button*=Select target Dir";

	it("select Origin Path", () => {
		return !app.client.click(selectOriginButton).waitUntilWindowLoaded(1000).element("#targetContainer").should.property.hidden;
	});
});
